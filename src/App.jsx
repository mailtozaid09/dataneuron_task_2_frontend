import React from 'react'

import HomePage from './pages/home/index.jsx';
import { ToastProvider } from './context/ToastContext.jsx';


const App = () => {

    return (
        <>
            <ToastProvider>
                <HomePage />
            </ToastProvider>
        </>
    )
}

export default App
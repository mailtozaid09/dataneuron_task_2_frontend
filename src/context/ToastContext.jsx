import React, { createContext, useState } from 'react';

export const ToastContext = createContext();

export const ToastProvider = ({ children }) => {
    const [title, setTitle] = useState('');
    const [subTitle, setSubTitle] = useState('');
    const [anchorOrigin, setAnchorOrigin] = useState({});
    const [isSuccess, setIsSuccess] = useState(true);

    const showToast = (title, isSuccess) => {
        setTitle(title);
        setAnchorOrigin(anchorOrigin);
        setIsSuccess(isSuccess);
        setTimeout(() => {
        setTitle('');
        setSubTitle('');
        }, 3000);
    };

  return (
    <ToastContext.Provider value={{ showToast }}>
        {children}
        {title && (
            // <View style={styles.container} >
            //     <View style={[styles.mainContainer, {backgroundColor: isSuccess ? colors.black : colors.white}, styles.shadowStyle]} >
            //         {isSuccess ?
            //         <Icon type="AntDesign" name="checkcircle" size={28} color={colors.white} />
            //         :
            //         <Icon type="AntDesign" name="exclamationcircle" size={28} color={colors.red} />
            //         }
            //         <Text numberOfLines={1} style={[styles.title, {color: isSuccess ? colors.white : colors.red}]} >{title}</Text>
            //     </View> 
            // </View> 
            <div>dsadsadsa</div>
        )}
    </ToastContext.Provider>
  );
};

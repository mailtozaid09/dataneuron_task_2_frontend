import React, { useState, useEffect, useContext } from 'react'
import './styles.css'

import ActionButton from '../../components/button';
import Analytics from '../../components/analytics';

import GenericTable from '../../components/table/GenericTable';
import AddModal from '../../components/modal/AddModal';
import EditModal from '../../components/modal/EditModal';
import DeleteModal from '../../components/modal/DeleteModal';

import { ToastContext } from '../../context/ToastContext';
import { addUser, editUser, deleteUser, getUsers, resetCount } from '../../services/apiRequests';

import useLoader from '../../components/loader/useLoader';
import Loader from '../../components/loader';



const HomePage = () => {

    const [showAddModal, setShowAddModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    
    const [count, setCount] = useState({});
    const [currentUser, setCurrentUser] = useState({});
    const [users, setUsers] = useState([]);

    const { isLoading, showLoader, hideLoader } = useLoader();
    const { showToast } = useContext(ToastContext);

    useEffect(() => {
        fetchUsers()
    }, [])
    

    const fetchUsers = async () => {
        showLoader()
        const onSuccess = (resp) => {
            setUsers(resp?.data);
            setCount(resp?.count)
            hideLoader()
        };

        const onError = (err) => {
            console.log("get users err > ",err?.response?.data?.message);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        getUsers(
            onSuccess,
            onError
        );
    };

    const addUserFunction = (body) => {
        showLoader()
        const onSuccess = (resp) => {
            hideLoader()
            fetchUsers()
        };

        const onError = (err) => {
            console.log("add users err > ",err?.response?.data?.message);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        addUser(
            body,
            onSuccess,
            onError
        );
    };


    const editUserFunction = (body) => {
        showLoader()
        const onSuccess = (resp) => {
            hideLoader()
            fetchUsers()
        };

        const onError = (err) => {
            console.log("edit users err > ",err?.response?.data?.message);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        editUser(
            body.id,
            body,
            onSuccess,
            onError
        );
    }


    const deleteUserFunction = (body) => {
        showLoader()
        const onSuccess = (resp) => {
            hideLoader()
            fetchUsers()
        };

        const onError = (err) => {
            console.log("delete users err > ",err?.response?.data?.message);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        deleteUser(
            body.id,
            onSuccess,
            onError
        );
    }

    const editModalFunction = (val) => {
        setCurrentUser(val)
        setShowEditModal(true)
    }
    
    const deleteModalFunction = (val) => {
        setCurrentUser(val)
        setShowDeleteModal(true)
    }

    const resentCountFunction = (body) => {
        showLoader()
        const onSuccess = (resp) => {
            hideLoader()
            fetchUsers()
        };

        const onError = (err) => {
            console.log("add users err > ",err?.response?.data?.message);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        resetCount(
            onSuccess,
            onError
        );
    };
    return (
        <div>
            <div>
                <div className='home_container' >
                    <Analytics count={count} resentCount={() => resentCountFunction()} />

                    <GenericTable
                        users={users}
                        editModalFunction={(val) => {editModalFunction(val)}}
                        deleteModalFunction={(val) => {deleteModalFunction(val)}}
                    />
                
                </div>
            </div>
            {showAddModal && <AddModal addUserFunction={(value) => {addUserFunction(value)}} onClose={() => {setShowAddModal(false)}} /> }
            {showEditModal && <EditModal currentUser={currentUser} editUserFunction={(value) => {editUserFunction(value)}} onClose={() => {setShowEditModal(false)}} /> }
            {showDeleteModal && <DeleteModal currentUser={currentUser} deleteUserFunction={(value) => {deleteUserFunction(value)}} onClose={() => {setShowDeleteModal(false)}} /> }

            
            {isLoading && <Loader /> }
            <ActionButton onClick={() => {setShowAddModal(true)}} />
        </div>
    )
}

export default HomePage
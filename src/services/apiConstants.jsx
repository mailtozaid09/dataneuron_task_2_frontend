export const apiRequest = {
    get: 'get',
    patch: 'patch',
    post: 'post',
    put: 'put',
    delete: 'delete',
};

export const apiHeaders = {
    contentType: 'content-type',
    authorization: 'authorization',
    typeFormData: 'multipart/form-data',
    acceptType: 'application/json',
    deviceType: 'device-type',
    web: 'Web',
    klipAuthToken: 'klip-auth-token',
    language: 'language',
    timezone: 'timezone',
    today: 'today',
    appVersion: 'appversion',
    deviceDetails: 'devicedetails',
    deviceUdid: 'device-udid',
};

export const pathUrls = {

    //users
    users: '/user',
    get_users: '/user',
    add_user: '/user',
    update_user: '/user/',
    delete_user: '/user/',
    reset_count: '/user/reset_count',
};

export const requestParams = {
    versionNo: 'versionNo',
    profilePicture: 'profilePicture',
    key: 'key',
    value: 'value',

    token: 'token',
    password: 'password',
    email: 'email',
};


export const endpoints = {
    //base_url: 'http://172.20.10.6:8080/api/v1/',
    base_url: 'https://dataneuron-task-2-backend.onrender.com/api/v1',
}
import { pathUrls } from './apiConstants';

import authFetch from './axios';

// User APIS

// Get All Users
export const getUsers = ( 
    onSuccess, onError, 
) => {
    return authFetch
        .get(pathUrls.users)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};


// Add User
export const addUser = ( 
    body, onSuccess, onError, 
) => {
    return authFetch
        .post(pathUrls.add_user, body)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};



// Edit User
export const editUser = ( 
    id, body, onSuccess, onError, 
) => {
    return authFetch
        .put(pathUrls.update_user + id, body)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// Delete User
export const deleteUser = ( 
    id, onSuccess, onError, 
) => {
    return authFetch
        .delete(pathUrls.delete_user + id)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};


// Delete User
export const resetCount = ( 
    onSuccess, onError, 
) => {
    return authFetch
        .post(pathUrls.reset_count)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

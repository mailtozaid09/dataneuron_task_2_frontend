import React, { useState, useEffect } from 'react';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';

import { Button, TextField } from '@mui/material';
import Input from '../input';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 350,
    bgcolor: 'background.paper',
    border: '1px solid #000',
    boxShadow: 24,
    borderRadius: 2,
    p: 4,
};


const EditModal = ({currentUser, onClose, editUserFunction}) => {

    const [open, setOpen] = useState(true);
    const handleClose = () => setOpen(false);


    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});

    useEffect(() => {
        console.log("currentUser> > >" ,currentUser);
        setForm(currentUser)
    }, [])
    

    const onChangeInput = ({name, value}) => {
        if (name == 'mobileNumber' && value.length > 10) {
          
        }else{
            setForm({...form, [name]: value})
            setErrors({})
        }       
    }

    const checkForValidations = () => {
        var isValid = true;

        if(!form.name){
            console.log("Please enter a valid full name!");
            isValid = false
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid full name!'}
            })
        }

        if(!form.userName){
            console.log("Please enter a valid username!");
            isValid = false
            setErrors((prev) => {
                return {...prev, userName: 'Please enter a valid username!'}
            })
        }

        if(!form.mobileNumber || form.mobileNumber.length < 10){
            console.log("Please enter a valid mobile number!");
            isValid = false
            setErrors((prev) => {
                return {...prev, mobileNumber: 'Please enter a valid mobile number!'}
            })
        }

        if(isValid){
            editUserFunction(form)
            onClose()
        }
    }

    return (
        <Modal
            open={open}
            onClose={() => {handleClose(); onClose()}}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2" style={{textAlign: 'center', }} sx={{mb: 2}} >
                    Update User
                </Typography>

                <Input
                    label="Full Name"
                    name="name"
                    placeholder="Eg. Zaid Ahmed"
                    error={errors.name}
                    value={form.name}
                    onChange={(name, value) => {onChangeInput(name, value); setErrors({}); }}
                />

                <Input
                    label="User Name"
                    name="userName"
                    placeholder="Eg. zaidahmed09"
                    error={errors.userName}
                    value={form.userName}
                    onChange={(name, value) => {onChangeInput(name, value); setErrors({}); }}
                />

                <Input
                    label="Mobile Number"
                    name="mobileNumber"
                    placeholder="Eg. 9123456780"
                    error={errors.mobileNumber}
                    value={form.mobileNumber}
                    onChange={(name, value) => {onChangeInput(name, value); setErrors({}); }}
                />

                <div style={{marginTop: 20, flexDirection: 'row', display: 'flex', alignItems: 'center', justifyContent: 'center'}} >
                    <Button 
                        fullWidth
                        sx={{mr: 1}}
                        variant="outlined"
                        onClick={() => {handleClose(); onClose()}}
                    >
                        Close
                    </Button>
                    <Button 
                        fullWidth
                        sx={{ml: 1}}
                        variant="contained"
                        onClick={() => {checkForValidations()}}
                    >
                        Update
                    </Button>
                </div>
            </Box>
        </Modal>
    );
}
export default EditModal
import React, { useState } from 'react';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';

import { Button, TextField } from '@mui/material';
import Input from '../input';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 350,
    bgcolor: 'background.paper',
    border: '1px solid #000',
    boxShadow: 24,
    borderRadius: 2,
    p: 4,
};


const DeleteModal = ({currentUser, onClose, deleteUserFunction}) => {

    const [open, setOpen] = useState(true);
    const handleClose = () => setOpen(false);

    return (
        <Modal
            open={open}
            onClose={() => {handleClose(); onClose()}}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2" style={{textAlign: 'center', }} sx={{mb: 2}} >
                    Delete User
                </Typography>

                <Typography id="modal-modal-description" sx={{}}>
                    Full Name: {currentUser?.name}
                </Typography>
                <Typography id="modal-modal-description" sx={{}}>
                    User Name: {currentUser?.userName}
                </Typography>
                <Typography id="modal-modal-description" sx={{}}>
                    Mobile Number: {currentUser?.mobileNumber}
                </Typography>
                

                <div style={{marginTop: 20, flexDirection: 'row', display: 'flex', alignItems: 'center', justifyContent: 'center'}} >
                    <Button 
                        fullWidth
                        sx={{mr: 1}}
                        variant="outlined"
                        onClick={() => {handleClose(); onClose()}}
                    >
                        Close
                    </Button>
                    <Button 
                        fullWidth
                        sx={{ml: 1}}
                        variant="contained"
                        style={{backgroundColor: "red",}}
                        onClick={() => {deleteUserFunction(currentUser); onClose()}}
                    >
                        Delete
                    </Button>
                </div>
            </Box>
        </Modal>
    );
}
export default DeleteModal
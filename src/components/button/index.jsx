import React from 'react'
import './button.css'

import { Fab } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

const ActionButton = ({title, onClick}) => {
    return (
        <div className='action_button_container' onClick={onClick}>
            <Fab color="primary" aria-label="add">
                <AddIcon />
            </Fab>
        </div>
    )
}

export default ActionButton
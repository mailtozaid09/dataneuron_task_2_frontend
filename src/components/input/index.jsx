import React from 'react'
import './button.css'

import { TextField } from '@mui/material';


const Input = ({label, placeholder, name, sx, style, error, value, onChange}) => {
    return (
        <div style={{marginBottom: 10}} >
            <TextField
                required
                id="standard-required"
                label={label}
                placeholder={placeholder}
                variant="standard"
                sx={[{width: '100%', mb: 1}, {sx}]}
                value={value}
                type={name == 'mobileNumber' ? 'number' : null}
                style={style}
                onChange={(e) => {onChange({name: name, value: e.target.value,}); }}
            />
            {error && <div style={{color: 'red'}} >{error}</div>}
        </div>
    )
}

export default Input
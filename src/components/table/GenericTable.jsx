import React, { useState, useEffect, useContext } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';

import DeleteIcon from '@mui/icons-material/Delete';
import BorderColorIcon from '@mui/icons-material/BorderColor';

import { ToastContext } from '../../context/ToastContext';
import Empty from '../empty';

const columns = [
    { id: 'name', label: 'Full Name', minWidth: 100 },
    { id: 'userName', label: 'User Name', minWidth: 100 },
    { id: 'mobileNumber', label: 'Mobile Number', minWidth: 100 },
    { id: 'update', label: 'Update', minWidth: 40 },
    { id: 'delete', label: 'Delete', minWidth: 40 },
];

export default function GenericTable({users, editModalFunction, deleteModalFunction}) {
    
    const { showToast } = useContext(ToastContext);
    
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <div style={{ width: '100%',}} >
            {users?.length != 0
            ?
            <div>
                <Paper sx={{marginTop: 10, width: '100%', overflow: 'hidden' }}>
                    <TableContainer sx={{ maxHeight: 440 }}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row) => {
                                return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                    {columns.map((column) => {
                                        const value = row[column.id];
                                        return (
                                            <TableCell key={column.id} align={column.align}>
                                                {value}

                                                {column.id == 'update' && (
                                                    <div onClick={() => {editModalFunction(row)}} > 
                                                        <BorderColorIcon style={{cursor: 'pointer'}} />
                                                    </div>
                                                )}


                                                {column.id == 'delete' && (
                                                    <div onClick={() => {deleteModalFunction(row)}} > 
                                                        <DeleteIcon style={{cursor: 'pointer'}} />
                                                    </div>
                                                )}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                                );
                            })}
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={users.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
                </div>
            :
            <div style={{ width: '100%', alignItems: 'center', justifyContent: 'center', display: 'flex'}} >
                <Empty /> 
            </div>
            }

        </div>
    );
}
import React from 'react'
import './styles.css'
import { Button } from '@mui/material';

const Analytics = ({count, resentCount }) => {
    console.log("count> > ",count);

    return (
        <div className='analytics_container' >
            <div className='analytics_card' >
                Add API Count: {count?.add_count}
            </div>
            <div className='analytics_card' >
                Update API Count: {count?.update_count}
            </div>
            <Button onClick={resentCount} variant="outlined" sx={{height: 54}} >
                Reset Count
            </Button>
        </div>
    )
}

export default Analytics
import React from 'react'

import EmptyImage from '../../assets/empty-box.png'

const Empty = ({}) => {
    return (
        <div style={{height: 300, width: 300, flexDirection: 'column', marginTop: 40, alignItems: 'center', justifyContent: 'center', display: 'flex',}} >
            <img src={EmptyImage} style={{height: 200 ,width: 200}} />
            <h1>No Data Found!!!</h1>
        </div>
    )
}

export default Empty
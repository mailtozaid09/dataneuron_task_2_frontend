import { useState } from 'react';

// Custom hook for managing loading state
const useLoader = () => {
  const [isLoading, setIsLoading] = useState(false);

  // Function to show loader
  const showLoader = () => {
    setIsLoading(true);
  };

  // Function to hide loader
  const hideLoader = () => {
    setIsLoading(false);
  };

  // Return loading state and functions to show/hide loader
  return { isLoading, showLoader, hideLoader };
};

export default useLoader;